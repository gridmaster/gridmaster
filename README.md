# Gridmaster

An SVG template to start with. 

Optimized for inches. 11x8.5 inch paper is the base. 

A 12"x12" outline for square foot projects. 

Made with Inkscape.